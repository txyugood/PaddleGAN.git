#  Copyright (c) 2020 PaddlePaddle Authors. All Rights Reserve.
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import argparse

import os
import paddle
from ppgan.apps.first_order_predictor import FirstOrderPredictor
from skimage import img_as_ubyte
import paddlehub as hub
import math
import cv2
import imageio
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--config", default=None, help="path to config")
parser.add_argument("--weight_path",
                    default=None,
                    help="path to checkpoint to restore")
parser.add_argument("--source_image", type=str, help="path to source image")
parser.add_argument("--driving_video", type=str, help="path to driving video")
parser.add_argument("--audio_file", type=str, help="path to audio file")
parser.add_argument("--output", default='output', help="path to output")
parser.add_argument("--relative",
                    dest="relative",
                    action="store_true",
                    help="use relative or absolute keypoint coordinates")
parser.add_argument(
    "--adapt_scale",
    dest="adapt_scale",
    action="store_true",
    help="adapt movement scale based on convex hull of keypoints")

parser.add_argument(
    "--find_best_frame",
    dest="find_best_frame",
    action="store_true",
    help=
    "Generate from the frame that is the most alligned with source. (Only for faces, requires face_aligment lib)"
)

parser.add_argument("--best_frame",
                    dest="best_frame",
                    type=int,
                    default=None,
                    help="Set frame to start from.")
parser.add_argument("--cpu", dest="cpu", action="store_true", help="cpu mode.")
parser.add_argument("--ratio", dest="ratio",type=str,default="1.0", help="area ratio of face")

parser.set_defaults(relative=False)
parser.set_defaults(adapt_scale=False)

if __name__ == "__main__":
    args = parser.parse_args()

    if args.cpu:
        paddle.set_device('cpu')
    cache_path = os.path.join(args.output,"cache")
    if not os.path.exists(cache_path):
        os.makedirs(cache_path)
    image_path = args.source_image
    
    origin_img = cv2.imread(image_path)
    image_width = origin_img.shape[1]
    image_hegiht = origin_img.shape[0]
    ratio = float(args.ratio)

    module = hub.Module(name="ultra_light_fast_generic_face_detector_1mb_640")
    face_detecions = module.face_detection(paths = [image_path], visualization=True, output_dir='face_detection_output')
    face_detecions = face_detecions[0]['data']
    
    face_list = []
    for i, face_dect in enumerate(face_detecions):
        left = math.ceil(face_dect['left'])
        right = math.ceil(face_dect['right'])
        top = math.ceil(face_dect['top'])
        bottom = math.ceil(face_dect['bottom'])
        width = right - left
        height = bottom - top
        center_x = left + width // 2
        center_y = top + height // 2
        size = math.ceil(ratio * height)

        new_left = max(center_x - size, 0)
        new_right = min(center_x + size, image_width)

        new_top = max(center_y - size, 0)
        new_bottom = min(center_y + size, image_hegiht)

        origin_img = cv2.imread(image_path)
        face_img = origin_img[new_top:new_bottom, new_left:new_right, :]
        face_height = face_img.shape[0]
        face_width = face_img.shape[1]

        cv2.imwrite(os.path.join(cache_path,'face_{}.jpeg'.format(i)), face_img)

        face_list.append({"path" : os.path.join(cache_path,'face_{}.jpeg'.format(i)),
                            "width":face_width, "height":face_height, 
                            "top":new_top, "bottom":new_bottom,
                            "left":new_left, "right":new_right, "center_x":center_x, "center_y":center_y,
                            "origin_width":width, "origin_height":height})
        
    frames = 0
    for face_dict in face_list:
        predictor = FirstOrderPredictor(output=args.output,
                                        weight_path=args.weight_path,
                                        config=args.config,
                                        relative=args.relative,
                                        adapt_scale=args.adapt_scale,
                                        find_best_frame=args.find_best_frame,
                                        best_frame=args.best_frame)
        predictions,fps = predictor.run(face_dict["path"], args.driving_video)
        face_dict['pre'] = predictions
        frames = len(predictions)
    images = []    
    for i in range(frames):
        new_frame = origin_img.copy()
        new_frame = new_frame[:,:,[2,1,0]]
        mask = np.zeros(origin_img.shape[:2]).astype('uint8')
        img_expand = np.zeros(origin_img.shape).astype('uint8')
        for j, face_dict in enumerate(face_list):
            pre = face_dict["pre"][i]
            face_width = face_dict["width"]
            face_height = face_dict["height"]
            top = face_dict["top"]
            bottom = face_dict["bottom"]
            left = face_dict["left"]
            right = face_dict["right"]
            img = cv2.resize(pre,(face_width, face_height))

            img_expand[top:bottom, left:right, :] = img_as_ubyte(img)

            center_x = face_dict["center_x"]
            center_y = face_dict["center_y"]
            origin_width = face_dict["origin_width"] 
            origin_height = face_dict["origin_height"]
            cv2.ellipse(mask, (int(center_x), int(center_y)), 
                        (math.floor(ratio * origin_width) - 1, math.floor(ratio * origin_height) - 1), 0, 0, 360,
                        (255,255,255), -1 ,8 ,0)
            # cv2.imwrite("face_{}.jpg".format(i), img_expand)
            # cv2.imwrite("test_{}.jpg".format(i), new_frame)
            #方形放置
            # new_frame[top:bottom, left:right, :] = img_as_ubyte(img)
        new_frame = cv2.UMat(new_frame)
        new_frame = cv2.copyTo(img_expand, mask, new_frame)
        if isinstance(new_frame, cv2.UMat):
            new_frame = new_frame.get()


        images.append(new_frame)
    imageio.mimsave(os.path.join(args.output, 'result.mp4'),
                [img_as_ubyte(frame) for frame in images],
                fps=fps)
    os.system("ffmpeg -y -i "  + os.path.join(args.output, 'result.mp4') + " -i " + args.audio_file +" -c:v copy -c:a aac -strict experimental " + os.path.join(args.output, 'mayiyahei.mp4'))
    